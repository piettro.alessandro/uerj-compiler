%{
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
//int yylex(void);
extern int yylex ();
int yyerror(char *s);

#define NADA		9999
#define FRACASSO	9998
#define ACHOUDIFVAR	9997

char *msg4 = "unknow entity in source program";
typedef enum {
	Variable,
	Constant,
	Temporary,
	Function,
	Procedure
} Entity;


/*
SymbTab: 1as 50 entradas p/ simbolos do fonte
e últimas p/ as temporarias
*/
typedef struct 
{
	char     asciiOfSource [20];
	Entity   entt;
	int      value;
} SymbTab;

typedef enum 
{
	Alocar,
	Referenciar
} AlocarOuReferenciar;

typedef enum {
	Variavel,
	Constante,
	Funcao
} Entidade;

SymbTab symbTab [100];

int	topTab=0;   // first 50 entries are programmer symbols
int	topTemp=50; // last  50 entries are temporary

//int searchSymbTab (YYSTYPE simb, Entidade vOUc, AlocarOuReferenciar aOUr){
int searchSymbTab (char* simb, Entidade vOUc, AlocarOuReferenciar aOUr){
	int k;
	for (k = 0;k < topTab; k++) 
	{
		switch(vOUc) 
		{
			case Variavel:
				if (strcmp(simb, symbTab[k].asciiOfSource)==0){return (k);}
				break;
			case Constante:
				if (atoi(simb) == symbTab[k].value)	return(k);
				break;
			default:
				break;
		}; // do switch
	}; // do for
	if (aOUr == Alocar||vOUc == Constante)	return (FRACASSO);
	else 
	{ // variável ausente, aborta !
		if (vOUc == Variavel)
		{
			printf("\n simbolo ausente %s \n",simb);
			exit(0);
		}; // do if Variavel
	}; // do else
}; // da funcao

//int insertSymbTab(YYSTYPE simb,Entidade vOUc)
int insertSymbTab(char* simb, Entidade vOUc)
{

	int retorno;
	retorno = searchSymbTab (simb, vOUc, Alocar);
	if (retorno == FRACASSO ) 
	{//simb deve
		if (vOUc == Constante){
			symbTab[topTab].value = atoi(simb);//vOUc eh Constante
		} else {//variável
			strcpy(symbTab[topTab].asciiOfSource,simb);
		}
		symbTab[topTab].entt = vOUc;
		retorno = topTab;
		topTab++;
		// de FRACASSO,incluido novo símbolo
		return (retorno);
	}
}

int temp () {
	char nomeTemporaria[4];
	int retorno;
	sprintf(nomeTemporaria,"t%d",topTemp-50);
	strcpy(symbTab[topTemp].asciiOfSource,nomeTemporaria);
	symbTab[topTemp].entt = Temporary;
	retorno=topTemp;
	topTemp++;
	return (retorno);
};

void printSymbTable () {
	int i, j, inicio, fimTrecho;
	inicio=0;
	j=0;
	fimTrecho = topTab-1;// trecho dos símbolos do programa

	while (j <= 1) 
	{
		for (i=inicio; i <= fimTrecho; i++) 
		{
			switch (symbTab[i].entt) {
				case Variable: printf("> Variable: ");break;
				case Constant: printf("> Numerical Constant: ");break;
				case Temporary: printf("> Temporary: ");break;
				case Function: printf("> Function: ");break;
				case Procedure: printf("> Procedure: ");break;
				default: yyerror(msg4);break;
			};
			printf("%s ", symbTab[i].asciiOfSource);
			printf("%d \n", symbTab[i].value);
		};// do for
		j++;
		inicio = 50;
		fimTrecho=topTemp-1;  // trecho das temporárias
	}; // do while
}; // da function printSymbTable

int prox =0;
typedef enum {
	ADD,
	SUB,
	MUL,
	DIV,
	STO,
	JF,
	J,
	PRINT
} Operador;

struct Quadrupla 
{
	Operador        op;
	int             operando1;
	int             operando2;
	int             operando3;
} quadrupla [ 100 ];




void gera (Operador codop,int end1,int end2,int end3){
	quadrupla [prox].op = codop;
	quadrupla [prox].operando1 = end1;
	quadrupla [prox].operando2 = end2;
	quadrupla [prox].operando3 = end3;
	prox++;
};

void remenda(int posM, Operador codop,int end1,int end2,int end3 ){
  	quadrupla [posM].op = codop;
  	quadrupla [posM].operando1 = end1;
  	quadrupla [posM].operando2 = end2;
  	quadrupla [posM].operando3 = end3;
};

void imprimeQuadrupla(){
  int r;
  for(r=0;r<prox;r++)
	printf("%d %d %d %d\n",
		quadrupla[r].op,
			quadrupla[r].operando1,
				quadrupla[r].operando2,
					quadrupla[r].operando3);

}; //da funcao imprimeQuadrupla

void finaliza () {
	printSymbTable();
	imprimeQuadrupla ();
	printf("End normal compilation! \n");
	exit(0);
};


int main(void)
{
   yyparse();
   return 0;
}

int yywrap(void)
{
   return 0;
}

int yyerror(char *errormsg)
{
    fprintf(stderr, "%s\n", errormsg);
    exit(1);
}

void atendeReclamacao () {
  int aux;
  aux = 0; // trying avoid compilation error in bison
  }

%}
%union
{
  	struct T
  	{
		char symbol[21];
		int intval;
  	    int indSimb;
    } t;
}

%token _ATRIB _EOF _ABREPAR _FECHAPAR _PTVIRG
%token _MAIS _MENOS _MULT _DIVID _PRINT
%token _IF _THEN _ELSE _WHILE _DO _LC _RC _COM _INT
%token _ERRO
%token _N _V
%type<t> P D C V B S E T F _N _V
//%type<t> E T F _N _V
%%
/*
regras da gramatica e acoes semanticas
*/
P    : D _LC C _RC
	 |  {
//			finaliza();	
		}
		;
D    :  D V _PTVIRG 
		{

		}
	 | V _PTVIRG 
	 	{
	 	
		}
	 ;
V	 : V _COM _V
		{
			$$.intval = $1.intval;
			$$.intval=insertSymbTab($3.symbol, Variable);
		}
	 | _INT _V
	 	{
			$$.intval=insertSymbTab($2.symbol, Variable);
		}
     ;
B 	 :  _LC C _RC
		{
			$$.intval = $2.intval;
		}
	 | S
	 	{
			$$.intval = $1.intval;
		}
     ;
C    : C _PTVIRG S
		{
			$$.intval = $3.intval;
		}
	| S {
			$$.intval = $1.intval;
		}
     ;
S	 : _IF _ABREPAR E _FECHAPAR _THEN B _ELSE B
//		S ( e ) t M B
		{
			//remenda($3.intval, JF, $3.indSimb, $6.intval+1, NADA);
			remenda($3.intval, JF,  $3.indSimb, prox,NADA);
			prox++;
			//remenda($6.intval, J, $3.symbol, prox,NADA);
		}
	| _WHILE _ABREPAR E _FECHAPAR _DO B
	  	{
			gera(J, $3.intval, NADA, NADA);
			remenda($3.intval, JF, $3.indSimb,prox,NADA);
			prox++;
	  	}
	|  _V _ATRIB E
		{
			$1.intval = insertSymbTab($1.symbol, Variable);
			gera(STO,$3.intval,$1.intval,NADA);
			printf("\n");
		}
	|  _PRINT _ABREPAR E _FECHAPAR
		{
			gera(PRINT,$3.intval, NADA, NADA);
			printf("\n");
		}
     
	 
E    : E _MAIS T
		{
			$$.intval = temp();
			gera (ADD,$1.intval,$3.intval,$$.intval);
		}
	| E _MENOS T
     	{
			$$.intval = temp();
			gera (SUB,$1.intval,$3.intval,$$.intval);}
	| T
        {
			$$.intval = $1.intval;
        }
T    : T _MULT F
		{
			$$.intval = temp();
			gera (MUL,$1.intval,$3.intval,$$.intval);
 	    }
	| T _DIVID F
     	{
			$$.intval = temp();
			gera (DIV,$1.intval,$3.intval,$$.intval);
		}
	| F
      	{
			$$.intval = $1.intval;
        }
F     : _ABREPAR E _FECHAPAR
        {
			$$.intval = $2.intval;
		}
F    : _V
        {
			$$.intval=insertSymbTab($1.symbol, Variable);
        }

	| _N
        {
			$$.intval=insertSymbTab($1.symbol, Constant);
        }
     ;
%%

