%{
#include "compile-uerj.tab.h"
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
char *yytext;

%}

ALFA 		    [A-Za-z_]
PLUS            [\+]
MINUS           [\-]
TIMES           [\*]
DIVIDE          [/]
DIGIT           [0-9]
NUMBER          ({DIGIT}+)
PRINT           "print"
IDENT			{ALFA}({ALFA}|{DIGIT})*
IF				"if"
THEN			"then"
ELSE			"else"
WHILE			"while"
DO				"do"
LC				"{"
RC				"}"
COM				","
INT				"int"
WS              [ \t]*
ATRIB			"="
LP              "("
RP              ")"
PTV             ";"
RET             [\n]

%%

{WS}            {
                /* eat up white space */
                }
{PLUS}          {
                return _MAIS;
                }
{TIMES}         {
                return _MULT;
                }
{MINUS}         {
                return _MENOS;
                }
{DIVIDE}        {
                return _DIVID;
                }
{ATRIB}			{
				return _ATRIB;
				}
{LP}            {
                return _ABREPAR;
                }
{RP}            {
                return _FECHAPAR;
                }
{PTV}			{
				return _PTVIRG;
				}
{NUMBER}        {
				strcpy(yylval.t.symbol, yytext);
                return _N;
                }
{PRINT}         {
				return _PRINT;
				}
{IDENT}         {
				strcpy(yylval.t.symbol, yytext);
				return _V;
				}
{IF}			{
				return _IF;
				}
{THEN}			{
				return _THEN;
				}
{ELSE}			{
				return _ELSE;
				}
{WHILE}			{
				return _WHILE;
				}
{DO}			{
				return _DO;
				}
{LC}			{
				return _LC;
				}
{RC}			{
				return _RC;
				}
{COM}			{
				return _COM;
				}
{INT}			{
				return _INT;
				}
{RET}           {
                return yytext[0];
                }
%%

/*
int main(void)
{
   yyparse();
   return 0;
}

int yywrap(void)
{
   return 0;
}

int yyerror(char *errormsg)
{
    fprintf(stderr, "%s\n", errormsg);
    exit(1);
}*/
